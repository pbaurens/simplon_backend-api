const Router = (server) => {
  //Our first route is here
  server.get('/', (request, response) => {
    response.json({ message: 'Hello world !' });
  });
};

export default Router;
